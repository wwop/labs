 <h1>---Установка nginx и изменение стартовой страницы---</h1>
Для этого необходимо создать папку в системе 

`mkdir nginx`

`cd nginx`

и создать там файл index.html с содержимым "nginx test111" с помощью команды:

`echo "nginx test111" > index.html`

После необходимо запустить контейнер nginx и подключить нашу созданную папку к внутренней папке в контейнере

`sudo docker run -d -p 80:80 --name nginx_test --restart always -v /home/arata/nginx:/usr/share/nginx/html nginx:stable`

Проверить можно с помощью браузера, вбив в строку поиска localhost:80 или с помощью терминала командой

`curl localhost`

<h1>---Установка apache и изменение стартовой страницы---</h1>
Производятся аналогичные действия как для nginx

`mkdir apache`

`cd apache`

`echo "apache test111" > index.html`

`sudo docker run -d -p 81:80 --name apache_test --restart always -v /home/arata/apache:/usr/local/apache2/htdocs httpd`

Проверяем с помощью браузера, вбив в строку поиска localhost:81



<h1>---Установка portainer---</h1>


`sudo docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer`

В браузере в строке поиска localhost:9000
log:admin
pass:admin12345678901

<h1>---Установка shipyard---</h1>

`docker run -ti -d --restart=always --name shipyard-rethinkdb rethinkdb`

`docker run -ti -d -p 4001:4001 -p 7001:7001 --restart=always --name shipyard-discovery microbox/etcd -name discovery`

`docker run -ti -d -p 2375:2375 --hostname=$HOSTNAME --restart=always --name shipyard-proxy -v /var/run/docker.sock:/var/run/docker.sock -e PORT=2375 shipyard/docker-proxy:latest`

`docker run -ti -d --restart=always --name shipyard-swarm-manager swarm:latest manage --host tcp://0.0.0.0:3375 etcd://127.0.0.1:4001`

`docker run -ti -d --restart=always --name shipyard-swarm-manager swarm:latest manage --host tcp://0.0.0.0:3375 etcd://127.0.0.1:4001`

`docker run -ti -d --restart=always --name shipyard-controller --link shipyard-rethinkdb:rethinkdb --link shipyard-swarm-manager:swarm -p 8080:8080 shipyard/shipyard:latest server -d tcp://swarm:3375`


<h1>---Установка registry---</h1>

Создаем docker-compose.yml

```
version: "2"
services:
  registry:
    image: registry:2
    environment:
      - REGISTRY_HTTP_SECRET=o43g2kjgn2iuhv2k4jn2f23f290qfghsdg
      - REGISTRY_STORAGE_DELETE_ENABLED=
    volumes:
      - ./registry-data:/var/lib/registry
  ui:
    image: jc21/registry-ui
    environment:
      - NODE_ENV=production
      - REGISTRY_HOST=registry:5000
      - REGISTRY_SSL=
      - REGISTRY_DOMAIN=
      - REGISTRY_STORAGE_DELETE_ENABLED=
    links:
      - registry
    restart: on-failure
  proxy:
    image: jc21/registry-ui-proxy
    ports:
      - 80:80
    depends_on:
      - ui
      - registry
    links:
      - ui
      - registry
    restart: on-failure

```

Переходим в директорию с этим файлом и вводим команду 

`sudo docker-compose up -d`

<h1>---Ограничение apache---</h1>

`docker run -d -p 8083:80 -m 50m --cpus 0.01 --name apache_limit apache`


<h1>---Dockerfile---<h1>
Создаем файл index.html с содержимым:

```html
<!DOCTYPE html>
'<html>'
'<head>'
        '<title>Test1</title>'
'</head>'

'<body>'
'<h1>One-page site</h1>'
'</body>'
'</html>'
```
и Dockerfile в этой же директории с содержимым:

```Dockerfile
FROM nginx:alpine
COPY . /usr/share/nginx/html
```

находясь в этой директории выполняем команду чтобы собрать контейнер из Dockerfile


`docker build -t site .`


далее запускаем созданный контейнер командой:


`docker run -itd -p 8082:80 --name onesite site`

Сайт будет доступен по адресу localhost:8082

<h1>Volumes</h1>

Примонтируем том к предыдущей работе командой:



`docker run -itd -p 8082:80 -v /home/arata/site1:/usr/share/nginx/html --name onesite site`


Сайт будет доступен по адресу localhost:8082/, изменяя на локальном диске файл index.html начальная страница сайта будет автоматически меняться
К примеру изменим файл таким образом:


```html
`<!DOCTYPE html>`
`<html>`
`<head>`
        `<title>Edited title</title>`
`</head>`

`<body>`
`<h1>Edited site</h1>`
`</body>`
`</html>`
```

Зайдя на сайт, сразу увидим изменения стартовой страницы.

--------------------------------------------------------
Создадим именованный том для PostgreSQL


`docker volume create postgres-data`


Создаем директорию init-scripts и в ней создаем файлы 
01_create_table1.sql

```sql
CREATE TABLE table1 (
        id SERIAL PRIMARY KEY,
        name VARCHAR(255),
        description TEXT
);    
```

02_create_table1.sql

```sql
CREATE TABLE table2 (
        id SERIAL PRIMARY KEY,
        title VARCHAR(255),
        value INT
);
```

Создаем docker-compose.yml

```yml
version: '3'

services:
  postgres:
    image: postgres:latest
    container_name: my-postgres-container
    environment:
      POSTGRES_USER: admin
      POSTGRES_PASSWORD: admin
      POSTGRES_DB: newdatabase
    volumes:
      - postgres-data:/var/lib/postgresql/data
      - ./init-scripts:/docker-entrypoint-initdb.d

volumes:
  postgres-data:



Запускаем контейнер командой:

```

`docker-compose up -d `



Проверим логи и убедимся что контейнер запущен

`docker logs my-postgres-container `

--------------------------------------------------------------------------------
Проверим том из примера 12, запустим контейнер

`docker run -itd -p 8083:80 --name primer12 primer`

Проинспектируем контейнер:

`docker inspect primer`

И видим:
```
"Volumes": {
                "/usr/share/nginx/html": {}
            },


```
<h1>---L7---</h1>

Создаем приложение app.py

```python
from flask import Flask, jsonify
from pymongo import MongoClient
import os

app = Flask(__name__)

MONGO_URI = os.environ.get('MONGO_URI')

client = MongoClient(MONGO_URI)
db = client['mydatabase']

@app.route('/')
def hello_world():
  return jsonify(message='Hello, World!')

if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0')


```
создаем Dockerfile

```Dockerfile
FROM python:3.10-slim
WORKDIR /app
COPY requirements.txt .
RUN pip install —no-cache-dir -r requirements.txt
COPY . .
USER 1000
CMD ["python", "app.py"]

```

для правильной работы нужно создать файл requirements.txt и написать туда все зависимости

```
Flask==2.0.1
pymongo==3.12.0
```

создаем файл с переменными для подключения к MongoDB:



`MONGO_URI=mongodb://your_username:your_password@mongodb_host:27017/db`



создаем docker-compose.yml

```yml
version: '3'

services:
  web:
    build:
    context: .
    ports:
      - "5000:5000"
    env_file:
      - .env
    depends_on:
      - mongodb

  mongodb:
    image: mongo:latest
    ports:
      - "27017:27017"

```

запускаем все это дело и получаем доступ по адресу localhost:5000


``docker-compose up --build``


<h1>---Elactic + Kibana install---</h1>

Для начала создаем сеть для этих контейнеров:

`docker network create elastic`


Запускаем оба контейнера
elastic:

`docker run -d --name elasticsearch --net elastic -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:8.11.3`



Kibana

`docker run -d --name kibana --net elastic -p 5601:5601 kibana:8.11.3`




Переходим в Kibana по адресу localhost:5601, начинается инициализация системы

![kib](https://gitlab.com/wwop/labs/-/raw/main/kibana.png?ref_type=heads)

Для начала сбросим пароль в elastic командой

`docker exec -it elasticsearch /usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic`

![pass](https://gitlab.com/wwop/labs/-/raw/main/password.png?ref_type=heads)

После получим token подключения и введем его в ui

`docker exec -it elasticsearch /usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s kibana`

![token](https://gitlab.com/wwop/labs/-/raw/main/token.png?ref_type=heads)


![req](https://gitlab.com/wwop/labs/-/raw/main/verifreq.png?ref_type=heads)

Далее необходимо получить verification code с помощью команды:

`docker exec -it kibana bin/kibana-verification-code`

![code](https://gitlab.com/wwop/labs/-/raw/main/verifcode.png?ref_type=heads)

В консоли выводится код и вводим его в ui

![1](https://gitlab.com/wwop/labs/-/raw/main/elastic.png?ref_type=heads)


![2](https://gitlab.com/wwop/labs/-/raw/main/elasticui.png?ref_type=heads)


<h1>---minikube---</h1>

Установка просиходит в 2 этапа
1. Получаем файлы командой:

`curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64`

2. Устанавливаем командой:

`sudo install minikube-linux-amd64 /usr/local/bin/minikube`

![install_picture](https://gitlab.com/wwop/labs/-/raw/main/1111.png?ref_type=heads)

Проверим версию:

`minikube version`

![version](https://gitlab.com/wwop/labs/-/raw/main/22222.png?ref_type=heads)
После установки minikube необходимо установить kubectl, инструмент для работы с кластером:

`curl -LO https://storage.googleapis.com/kubernetes-release/release/curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt/bin/linux/amd64/kubectl`

Далее необходимо дать право запуска и переместить в /usr/local/bin

`chmod +x kubectl`

`sudo mv kubectl /usr/local/bin/`

После проверим версию kubectl командой:

`kubectl version -o yaml`

![kube](https://gitlab.com/wwop/labs/-/raw/main/333.png?ref_type=heads)

После установки необходимо запустить кластер командой:

`minikube start --driver=docker`

![cluster](https://gitlab.com/wwop/labs/-/raw/main/4444.png?ref_type=heads)

Проверим статус нашего кластера командой:

`kubectl cluster-info`

![clusterinfo](https://gitlab.com/wwop/labs/-/raw/main/555.png?ref_type=heads)

Создадим простой nginx деплой:

`kubectl create deployment nginx-web --image=nginx`

`kubectl expose deployment nginx-web --type NodePort --port=80`

`kubectl get deployment,pod,svc`

![deploy](https://gitlab.com/wwop/labs/-/raw/main/666.png?ref_type=heads)

Для удобства работы с k8s можно использовать аддон в minikube под названием dashboard

`minikube addons enable dashboard`

![dash](https://gitlab.com/wwop/labs/-/raw/main/dash.png?ref_type=heads)

Открыть dashboard можно используя команду:

`minikube dashboard`

![dash_op](https://gitlab.com/wwop/labs/-/raw/main/dasshboard%20open.png?ref_type=heads)

Интерфейс выглядит следующим образом:

![ui](https://gitlab.com/wwop/labs/-/raw/main/ui.png?ref_type=heads)
